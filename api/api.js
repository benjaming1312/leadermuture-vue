var api = [
    // 一筆國家的資料
    {
        id: '1',
        state: '亞洲',
        country: '中國',
        city: '廣州',
        searchname: ['CANTON','CAN','廣州','广州'],
        companyname: 'Leader Mutual Freight System Inc.(Chongqing)',
        address: 'Room 2212, Tower 3, Zhongyu Midtown, No.86, Hongjin Road, Longxi Street, Yubei District, Chongqing, China.401120',
        tel: '86 23 8606 7220/7221',
        fax: '86 23 8606 7220 EXT.806',
        pic: 'MS.Sherry Peng /Station Manager',
        mail: 'ckg.obd@leader-mutual.com.cn',
        map: 'http://f.amap.com/5QJ8I_0021GGD'
    },
    {
        id: '2',
        state: '亞洲',
        country: '台灣',
        city: '台北',
        searchname: ['Taipei','TPE','台北','台北'],
        companyname: 'Leader Mutual Freight System Inc.(Chongqing)',
        address: 'Room 2212, Tower 3, Zhongyu Midtown, No.86, Hongjin Road, Longxi Street, Yubei District, Chongqing, China.401120',
        tel: '86 23 8606 7220/7221',
        fax: '86 23 8606 7220 EXT.806',
        pic: 'MS.Sherry Peng /Station Manager',
        mail: 'ckg.obd@leader-mutual.com.cn',
        map: 'http://f.amap.com/5QJ8I_0021GGD'
    },
    {
        id: '3',
        state: '亞洲',
        country: '台灣',
        city: '高雄',
        searchname: ['Kaoshuing','KAH','高雄','高雄'],
        companyname: 'Leader Mutual Freight System Inc.(Chongqing)',
        address: 'Room 2212, Tower 3, Zhongyu Midtown, No.86, Hongjin Road, Longxi Street, Yubei District, Chongqing, China.401120',
        tel: '86 23 8606 7220/7221',
        fax: '86 23 8606 7220 EXT.806',
        pic: 'MS.Sherry Peng /Station Manager',
        mail: 'ckg.obd@leader-mutual.com.cn',
        map: 'http://f.amap.com/5QJ8I_0021GGD'
    },
    {
        id: '4',
        state: '美洲',
        country: '台灣',
        city: '台北',
        searchname: ['CANTON','CAN','廣州','广州'],
        companyname: 'Leader Mutual Freight System Inc.(Chongqing)',
        address: 'Room 2212, Tower 3, Zhongyu Midtown, No.86, Hongjin Road, Longxi Street, Yubei District, Chongqing, China.401120',
        tel: '86 23 8606 7220/7221',
        fax: '86 23 8606 7220 EXT.806',
        pic: 'MS.Sherry Peng /Station Manager',
        mail: 'ckg.obd@leader-mutual.com.cn',
        map: 'http://f.amap.com/5QJ8I_0021GGD'
    },
    {
        id: '5',
        state: '非洲',
        country: '台灣',
        city: '台北',
        searchname: ['CANTON','CAN','廣州','广州'],
        companyname: 'Leader Mutual Freight System Inc.(Chongqing)',
        address: 'Room 2212, Tower 3, Zhongyu Midtown, No.86, Hongjin Road, Longxi Street, Yubei District, Chongqing, China.401120',
        tel: '86 23 8606 7220/7221',
        fax: '86 23 8606 7220 EXT.806',
        pic: 'MS.Sherry Peng /Station Manager',
        mail: 'ckg.obd@leader-mutual.com.cn',
        map: 'http://f.amap.com/5QJ8I_0021GGD'
    }
]