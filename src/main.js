import Vue from 'vue'
import App from './App.vue'
import Globe from './components/globe.vue'

// new Vue({
//   el: '#app',
//   render: h => h(App)
// })

new Vue({
  el: '#app',
  render: h => h(Globe)
})
